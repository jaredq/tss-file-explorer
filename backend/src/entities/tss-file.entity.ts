import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity({
  name: 'file',
})
export class TssFile {
  constructor() {}

  @PrimaryGeneratedColumn({
    type: 'bigint',
  })
  id: number;

  @Column({
    type: 'bigint',
  })
  parent: number;

  @Column({
    type: 'bigint',
    array: true,
    default: '{}',
  })
  parents: number[];

  @Column({ type: 'text', nullable: false })
  name: string;

  @Column({ type: 'text', nullable: true })
  content: string;

  @Column({
    type: 'varchar',
    length: 10,
    nullable: false,
    default: 'file',
  })
  @Index()
  type: string;
}
