import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http-exception.filter';

const {
  SERVER_PORT = '3001',
} = process.env;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(SERVER_PORT || 3000, () =>
    console.info(`Listening on port ${SERVER_PORT} ...`),
  );
}
bootstrap();
