import { DataSource } from 'typeorm';
import { default as ormconfig } from './ormconfig';

const config: DataSource = new DataSource(ormconfig);

export default config;
