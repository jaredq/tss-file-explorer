export class CommonResponse {
  success: boolean = false;
  errorCode?: number;
  errorMessage?: string;
}
