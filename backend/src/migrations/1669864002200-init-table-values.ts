import { MigrationInterface, QueryRunner } from "typeorm";

export class initTables1669864002200 implements MigrationInterface {
    name = 'initTables1669864002200'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO public.file (id,parent,parents,name,"content","type") VALUES (0,-1,'{}','root',NULL,'folder')`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM public.file where id = 0`);
    }

}
