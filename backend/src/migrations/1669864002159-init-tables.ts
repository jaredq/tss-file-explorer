import { MigrationInterface, QueryRunner } from "typeorm";

export class initTables1669864002159 implements MigrationInterface {
    name = 'initTables1669864002159'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "file" ("id" BIGSERIAL NOT NULL, "parent" bigint NOT NULL, "parents" bigint array NOT NULL DEFAULT '{}', "name" text NOT NULL, "content" text, "type" character varying(10) NOT NULL DEFAULT 'file', CONSTRAINT "PK_36b46d232307066b3a2c9ea3a1d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_65593ce703593144d5a8f5fddf" ON "file" ("type") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_65593ce703593144d5a8f5fddf"`);
        await queryRunner.query(`DROP TABLE "file"`);
    }

}
