import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class ErrorHandlerMiddleware implements NestMiddleware {
  private logError = (error: Error) => console.error(`Request failed: `, error);

  async use(req: any, res: any, next: () => void) {
    const { error } = req;
    if (error) {
      const { status = 500, statusText = 'Internal Server Error' } = error;
      this.logError(error);
      res.status(status).json({ message: statusText });
    }
    next();
  }
}
