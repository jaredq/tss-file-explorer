import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    let errorMessage = exception.message;

    if (status == HttpStatus.NOT_FOUND) {
      errorMessage = 'Not Found';
    }

    response.status(status).json({
      success: false,
      errorMessage,
    });
  }
}
