import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { default as ormconfig } from './ormconfig';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ErrorHandlerMiddleware } from './middlewares/error-handler.middleware';
import { FileExplorerController } from './file-explorer/file-explorer.controller';
import { FileExplorerService } from './file-explorer/file-explorer.service';
import { TssFile } from './entities';

@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    TypeOrmModule.forFeature([TssFile]),
  ],
  controllers: [AppController, FileExplorerController],
  providers: [AppService, FileExplorerService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ErrorHandlerMiddleware)
      .forRoutes({ path: '/**', method: RequestMethod.ALL });
  }
}
