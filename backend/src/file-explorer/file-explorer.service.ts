import { Logger, Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TssFile } from 'src/entities';
import {
  CreateFileInput,
  CreateFileOutput,
  GetFileInput,
  GetFileOutput,
  GetFilesInput,
  GetFilesOutput,
  SearchFilesInput,
  SearchFilesOutput,
} from './dto';

@Injectable()
export class FileExplorerService {
  private readonly logger = new Logger(FileExplorerService.name);

  constructor(
    @InjectRepository(TssFile)
    private readonly tssFileRepo: Repository<TssFile>,
  ) {}

  async createFile(payload: CreateFileInput): Promise<CreateFileOutput> {
    this.logger.debug('createFile', payload);
    try {
      const parent = payload.parent || 0;
      let parents: number[];
      if (parent > 0) {
        // check parent's existence and if its type is folder
        const parentFile = await this.tssFileRepo.findOneBy({ id: parent });
        if (!parentFile || parentFile.type !== 'folder') {
          throw new HttpException(
            'Parent does not exist or is not a folder.',
            HttpStatus.BAD_REQUEST,
          );
        } else {
          parents = [...parentFile.parents, parent];
        }
      } else {
        parents = [parent];
      }

      const result = await this.tssFileRepo.save(
        this.tssFileRepo.create({
          name: payload.name,
          content: payload.content,
          type: payload.type,
          parent,
          parents,
        }),
      );
      return { data: result };
    } catch (e) {
      this.logger.error('createFile error', e);
      throw new HttpException('Failed to create file.', HttpStatus.BAD_REQUEST);
    }
  }

  async getFiles(payload: GetFilesInput): Promise<GetFilesOutput> {
    this.logger.debug('getFiles', payload);
    try {
      const parent = payload.parent || 0;
      if (parent > 0) {
        // check parent's existence and if its type is folder
        const parentFile = await this.tssFileRepo.findOneBy({ id: parent });
        if (!parentFile || parentFile.type !== 'folder') {
          throw new HttpException(
            'Parent does not exist or is not a folder.',
            HttpStatus.BAD_REQUEST,
          );
        }
      }

      const result = await this.tssFileRepo.findBy({
        parent: payload.parent,
      });
      return { data: result };
    } catch (e) {
      this.logger.error('getFiles error', e);
      throw new HttpException('Failed to get files.', HttpStatus.BAD_REQUEST);
    }
  }

  async getFile(payload: GetFileInput): Promise<GetFileOutput> {
    this.logger.debug('getFile', payload);
    try {
      const id = payload.id || 0;
      const result = await this.tssFileRepo.findOneBy({ id });
      if (!result) {
        throw new HttpException(
          'File/folder does not exist.',
          HttpStatus.BAD_REQUEST,
        );
      }
      return { data: result };
    } catch (e) {
      this.logger.error('getFiles error', e);
      throw new HttpException('Failed to get file.', HttpStatus.BAD_REQUEST);
    }
  }

  async searchFiles(payload: SearchFilesInput): Promise<SearchFilesOutput> {
    this.logger.debug('searchFiles', payload);
    try {
      const keyword = payload.keyword;

      const result = await this.tssFileRepo
        .createQueryBuilder('tf')
        .where('tf.name ilike :name', { name: `%${keyword}%` })
        .getMany();
      return { data: result };
    } catch (e) {
      this.logger.error('searchFiles error', e);
      throw new HttpException('Failed to search files.', HttpStatus.BAD_REQUEST);
    }
  }
}
