import { Test, TestingModule } from '@nestjs/testing';
import { FileExplorerService } from './file-explorer.service';

describe('FileExplorerService', () => {
  let service: FileExplorerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileExplorerService],
    }).compile();

    service = module.get<FileExplorerService>(FileExplorerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
