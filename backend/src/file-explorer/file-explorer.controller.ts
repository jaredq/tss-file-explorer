import { Controller, Get, Post, Body, Param, Query } from '@nestjs/common';
import { FileExplorerService } from './file-explorer.service';
import {
  CreateFileInput,
  CreateFileReq,
  CreateFileResp,
  GetFileInput,
  GetFileReq,
  GetFileResp,
  GetFilesInput,
  GetFilesReq,
  GetFilesResp,
  SearchFilesInput,
  SearchFilesReq,
  SearchFilesResp,
} from './dto';

@Controller('file-explorer')
export class FileExplorerController {
  constructor(private fileExplorerService: FileExplorerService) {}

  @Post('files')
  public async createFile(
    @Body() body: CreateFileReq,
  ): Promise<CreateFileResp> {
    if (!body.name) {
      return {
        success: false,
        errorMessage: 'Name is required.',
      };
    }
    if (['file', 'folder'].indexOf(body.type) < 0) {
      return {
        success: false,
        errorMessage: 'Type is invalid.',
      };
    }

    // TODO check the other params' length, etc

    const payload: CreateFileInput = { ...body };
    const { data } = await this.fileExplorerService.createFile(payload);

    return {
      success: true,
      data,
    };
  }

  @Get('files')
  public async getFiles(@Query() query: GetFilesReq): Promise<GetFilesResp> {
    const payload: GetFilesInput = { parent: query.parent || 0 };
    const { data } = await this.fileExplorerService.getFiles(payload);

    return {
      success: true,
      data,
    };
  }

  @Get('files/:id')
  public async getFile(@Param() params: GetFileReq): Promise<GetFileResp> {
    const payload: GetFileInput = { id: params.id };
    const { data } = await this.fileExplorerService.getFile(payload);

    return {
      success: true,
      data,
    };
  }

  @Get('search')
  public async searchFiles(
    @Query() query: SearchFilesReq,
  ): Promise<SearchFilesResp> {
    const keyword = query.keyword;
    if (!keyword) {
      return {
        success: false,
        errorMessage: 'Keyword is required.',
      };
    }

    // TODO check sensitive letters in keyword

    const payload: SearchFilesInput = { keyword: query.keyword };
    const { data } = await this.fileExplorerService.searchFiles(payload);

    return {
      success: true,
      data,
    };
  }
}
