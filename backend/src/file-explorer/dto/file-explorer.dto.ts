import { CommonResponse } from './../../shared/dto/common-response.dto';
export class FileItem {
  name: string;
  type: string;
  content?: string;
  id: number;
  parent?: number;
  parents?: number[];
}
