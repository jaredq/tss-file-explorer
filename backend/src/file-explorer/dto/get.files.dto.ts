import { CommonResponse } from '../../shared/dto/common-response.dto';
import { FileItem } from './file-explorer.dto';

export class GetFilesReq {
  parent?: number;
}

export class GetFilesResp extends CommonResponse {
  data: FileItem[];
}

export class GetFilesInput {
  parent?: number;
}

export class GetFilesOutput {
  data: FileItem[];
}