import { CommonResponse } from '../../shared/dto/common-response.dto';
import { FileItem } from './file-explorer.dto';

export class SearchFilesReq {
  keyword: string;
}

export class SearchFilesResp extends CommonResponse {
  data?: FileItem[];
}

export class SearchFilesInput {
  keyword: string;
}

export class SearchFilesOutput {
  data: FileItem[];
}