import { CommonResponse } from '../../shared/dto/common-response.dto';
import { FileItem } from './file-explorer.dto';

export class GetFileReq {
  id: number;
}

export class GetFileResp extends CommonResponse {
  data: FileItem;
}

export class GetFileInput {
  id: number;
}

export class GetFileOutput {
  data: FileItem;
}
