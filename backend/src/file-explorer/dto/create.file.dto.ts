import { CommonResponse } from '../../shared/dto/common-response.dto';
import { FileItem } from './file-explorer.dto';

export class CreateFileReq {
  name: string;
  type: string;
  content?: string;
  parent?: number;
}

export class CreateFileResp extends CommonResponse {
  data?: FileItem;
}

export class CreateFileInput {
  name: string;
  type: string;
  content?: string;
  parent?: number;
}

export class CreateFileOutput {
  data: FileItem;
}
