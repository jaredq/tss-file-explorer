import axios from "axios";

const apiHost = process.env.API_HOST || "http://localhost:3001";

export function createFile(
  name: string,
  type: string,
  parent?: string,
  content?: string
) {
  return axios.post(`${apiHost}/file-explorer/files`, {
    name,
    type,
    parent,
    content,
  });
}

export function getFiles(parent: string) {
  return axios.get(`${apiHost}/file-explorer/files?parent=${parent || 0}`);
}

export function getFile(id: string) {
  return axios.get(`${apiHost}/file-explorer/files/${id || 0}`);
}

export function searchFiles(keyword: string) {
  return axios.get(`${apiHost}/file-explorer/search?keyword=${keyword}`);
}
