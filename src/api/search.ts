import axios from "axios";

const apiHost = process.env.API_HOST || "http://localhost:3001";

export function search(keyword: string) {
  return axios.get(`${apiHost}/file-explorer/search?keyword=${keyword}`);
}
