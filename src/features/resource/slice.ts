import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { Resource } from "../../types";
import { createFile, getFile, getFiles, searchFiles } from "../../api/explorer";

export interface ResourceState {
  currentId: string | undefined;
  all: Resource[];
}

const initialState: ResourceState = {
  currentId: undefined,
  all: [],
};

export const fetchCreateFile = createAsyncThunk(
  "file-explorer/fetchCreateFile",
  async (
    payload: { name: string; type: string; parent?: string; content?: string },
    thunkAPI
  ) => {
    const { name, type, parent, content } = payload;
    const response = await createFile(name, type, parent, content);
    return response.data;
  }
);

export const fetchGetFileById = createAsyncThunk(
  "file-explorer/fetchGetFileById",
  async (id: string, thunkAPI) => {
    let response = await getFile(id);
    const { data } = response.data;
    if (data.type === "folder") {
      response = await getFiles(data.id);
    }
    return response.data;
  }
);

export const fetchGetFiles = createAsyncThunk(
  "file-explorer/fetchGetFiles",
  async (parent: string, thunkAPI) => {
    const response = await getFiles(parent);
    return response.data;
  }
);

export const fetchSearchFiles = createAsyncThunk(
  "file-explorer/fetchSearchFiles",
  async (keyword: string, thunkAPI) => {
    const response = await searchFiles(keyword);
    return response.data;
  }
);

export const resourceSlice = createSlice({
  name: "resource",
  initialState,
  reducers: {
    addResource(state, action: PayloadAction<Resource>) {
      state.all.push(action.payload);
    },
    setCurrent(state, action: PayloadAction<string | undefined>) {
      state.currentId = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCreateFile.fulfilled, (state, action) => {
      state.all.push(action.payload.data);
      state.currentId = action.payload.data.id || "0";
    });
    builder.addCase(fetchGetFileById.fulfilled, (state, action) => {
      let all = state.all || [];
      if (Array.isArray(action.payload.data)) {
        const parent = action.meta.arg || "0";
        all = all.filter((item) => item.parent !== parent);
        all = all.concat(action.payload.data);
      } else {
        const id = action.meta.arg || "0";
        all = all.filter((item) => item.id !== id);
        all.push(action.payload.data);
      }

      state.all = all;
      state.currentId = action.meta.arg || "0";
    });
    builder.addCase(fetchGetFiles.fulfilled, (state, action) => {
      let all = state.all || [];

      const parent = action.meta.arg || "0";
      all = all.filter((item) => item.parent !== parent);
      all.push(action.payload.data);

      state.all = all;
      state.currentId = action.meta.arg || "0";
    });
    builder.addCase(fetchSearchFiles.fulfilled, (state, action) => {
      state.all = action.payload.data;
      state.currentId = undefined;
    });
  },
});

export const { addResource, setCurrent } = resourceSlice.actions;

export default resourceSlice.reducer;
