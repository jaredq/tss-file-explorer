# Create File

* POST <http://localhost:3001/file-explorer/files>

## Request

### Body

```json
{
  "name": "name of file or folder",
  "type": "file or folder",
  "parent": "id of parent",
  "content": "content of file"
}
```

## Success Response

```json
{
  "success": true,
  "data": {
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }
}
```

# Get File

* GET <http://localhost:3001/file-explorer/files/:id>

## Request

### Paramters

* id

## Success Response

```json
{
  "success": true,
  "data": {
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }
}
```

# Get Files

* GET <http://localhost:3001/file-explorer/files?parent=:parent>

## Request

### Paramters

* parent

## Success Response

```json
{
  "success": true,
  "data": [{
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }, {
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }]
}
```

# Search Files

* GET <http://localhost:3001/file-explorer/search?keyword=:keyword>

## Request

### Paramters

* keyword

## Success Response

```json
{
  "success": true,
  "data": [{
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }, {
    "name": "name of file or folder",
    "type": "file or folder",
    "parent": "id of parent",
    "content": "content of file",
    "parents": ["id of parent lvl 1", "id of parent lvl 2", "id of parent lvl 3"]
  }]
}
```
